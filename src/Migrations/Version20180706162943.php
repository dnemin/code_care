<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180706162943 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
         $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");

         $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_64C19C1989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
         $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, title VARCHAR(255) NOT NULL, INDEX IDX_D34A04AD12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
         $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');

         $this->addSql("
                    INSERT INTO `category` (`id`, `title`, `slug`) VALUES
                    (9, 'Катергория 1', 'katergoriya_1'),
                    (10, 'Катергория 2', 'katergoriya_2'),
                    (11, 'Катергория 3', 'katergoriya_3'),
                    (12, 'Катергория 4', 'katergoriya_4'),
                    (13, 'Катергория 5', 'katergoriya_5');
                   ");
      $this->addSql("
                    INSERT INTO `product` (`id`, `category_id`, `title`) VALUES
                    (1, 9, 'Продукт 1'),
                    (2, 10, 'Продукт 2'),
                    (3, 11, 'Продукт 3'),
                    (4, 12, 'Продукт 4'),
                    (5, 13, 'Продукт 5'),
                    (6, 9, 'Продукт 1'),
                    (7, 10, 'Продукт 2'),
                    (8, 11, 'Продукт 3'),
                    (9, 12, 'Продукт 4'),
                    (10, 13, 'Продукт 5'),
                    (11, 9, 'Продукт 6'),
                    (12, 10, 'Продукт 7'),
                    (13, 11, 'Продукт 8'),
                    (14, 12, 'Продукт 9'),
                    (15, 13, 'Продукт 10'),
                    (16, 9, 'Продукт 11'),
                    (18, 11, 'Продукт 13'),
                    (19, 12, 'Продукт 14'),
                    (20, 13, 'Продукт 15'),
                    (21, 9, 'Продукт 16'),
                    (22, 10, 'Продукт 17'),
                    (23, 11, 'Продукт 18'),
                    (24, 12, 'Продукт 19'),
                    (26, 9, 'Продукт 21'),
                    (27, 10, 'Продукт 22'),
                    (29, 12, 'Продукт 24'),
                    (31, 9, 'Продукт 26');
         ");


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
