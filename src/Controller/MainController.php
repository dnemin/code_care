<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, ProductRepository $productRepository)
    {
        $products = $productRepository->getAllProducts();

         $paginator  = $this->get('knp_paginator');
         $pagination = $paginator->paginate(
             $products,
             $request->query->getInt('page', 1),
             9
         );

        return $this->render('main/index.html.twig', compact('pagination'));
    }
}
