<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CategoryController extends Controller
{
     /**
      * @Route("/category", name="category_list")
      * @Route("/category/{category_slug}", name="current_category")
      * @ParamConverter("category", options={"mapping": {"category_slug": "slug"}})
      */
     public function index(Category $category = null)
     {
          $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

          return $this->render('category/index.html.twig', compact('categories', 'category'));
     }


}
