<?php

namespace App\Admin;

use App\Entity\Category;
use Cocur\Slugify\Slugify;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;

class CategoryAdmin extends AbstractAdmin
{
     protected function configureFormFields(FormMapper $formMapper)
     {
          $formMapper->add('title', TextType::class);
     }

     protected function configureDatagridFilters(DatagridMapper $datagridMapper)
     {
          $datagridMapper->add('title');
     }

     protected function configureListFields(ListMapper $listMapper)
     {
          $listMapper->addIdentifier('title');
     }


     public function preValidate($category)
     {
          $slugify = new Slugify();
          $slug = $slugify->slugify($category->getTitle(), '_');
          $existsCategory = $this->getConfigurationPool()->getContainer()->get('doctrine')
              ->getManager()->getRepository(Category::class)->findBy(['slug' => $slug]);
          if ($existsCategory) {
               $this->getForm()->addError(new FormError("Slug {$slug} already exist"));
          }
          $category->setSlug($slug);
     }
}