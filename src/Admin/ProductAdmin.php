<?php

namespace App\Admin;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProductAdmin extends AbstractAdmin
{
     protected function configureFormFields(FormMapper $formMapper)
     {
          $formMapper->add('title', TextType::class);
          $formMapper->add('category', EntityType::class,
              [
                  'class' => Category::class,
                  'choice_label' => 'title',
              ]);
     }

     protected function configureDatagridFilters(DatagridMapper $datagridMapper)
     {
          $datagridMapper->add('title');
     }

     protected function configureListFields(ListMapper $listMapper)
     {
          $listMapper->addIdentifier('title');
     }
}